package main

import (
	"encoding/json"
	"log"

	"github.com/streadway/amqp"
)

func publishMessage(message []byte) error {
	transaction := Transaction{}
	err := json.Unmarshal(message, &transaction)
	if err != nil {
		return err
	}

	conn, err := amqp.Dial("amqp://guest:guest@localhost/")
	if err != nil {
		return err
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		return err
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"transaction_queue",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	body, err := json.Marshal(transaction)
	if err != nil {
		return err
	}

	err = ch.Publish(
		"",
		q.Name,
		false,
		false,
		amqp.Publishing{
			ContentType:  "application/json",
			Body:         body,
			DeliveryMode: amqp.Persistent,
		},
	)
	if err != nil {
		return err
	}

	log.Printf(" [x] Transaction sent %s\n", string(body))
	return nil
}

func readMessage() error {
	conn, err := amqp.Dial("amqp://guest:guest@localhost/")
	if err != nil {
		return err
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		return err
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"transaction_queue",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := ch.Consume(
		q.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	go func() {
		for d := range msgs {
			body := d.Body
			transaction := Transaction{}
			err := json.Unmarshal(body, &transaction)
			if err != nil {
				log.Printf("Error decoding JSON: %v", err)
				continue
			}

			balanceAfter, err := updateBalance(transaction)
			if err != nil {
				log.Printf("Error updating balance: %v", err)
				continue
			}

			log.Printf(" [x] Transaction done. Balance after transaction: %d", balanceAfter.Balance)
		}
	}()

	return nil
}
