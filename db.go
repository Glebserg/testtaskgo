package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

func getEnvVar(key string) string {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatal(err)
	}
	return os.Getenv(key)
}

type Transaction struct {
	Name        string
	Transaction int
}

type Balance struct {
	Balance int
}

type BalanceAfterTransaction struct {
	ID      int
	Name    string
	Balance int
}

var (
	host      = getEnvVar("DB_HOST")
	port, err = strconv.Atoi(getEnvVar("DB_PORT"))
	user      = getEnvVar("DB_USER")
	password  = getEnvVar("DB_PASSWORD")
	dbname    = getEnvVar("DB_NAME")
)

func checkBalance(query Transaction) (Balance, error) {
	connStr := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return Balance{}, err
	}
	defer db.Close()

	balance := Balance{}

	err = db.QueryRow("SELECT balance FROM public.users WHERE name = $1", query.Name).Scan(&balance.Balance)
	if err != nil {
		return Balance{}, err
	}

	return balance, nil
}

func updateBalance(cmd Transaction) (BalanceAfterTransaction, error) {
	connStr := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return BalanceAfterTransaction{}, err
	}
	defer db.Close()

	user := BalanceAfterTransaction{}

	err = db.QueryRow("UPDATE public.users SET balance = balance + $1 WHERE name = $2 returning id, name, balance", cmd.Transaction, cmd.Name).Scan(&user.ID, &user.Name, &user.Balance)
	if err != nil {
		return BalanceAfterTransaction{}, err
	}

	return user, nil
}
