package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"log"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
)

var (
	ErrInsufficientFunds = errors.New("insufficient funds")
	ErrUserNotFound      = errors.New("user not found")
)

func main() {

	log.Printf(" [~] START \n")

	r := router.New()

	r.POST("/transaction", handleTransaction)

	fasthttp.ListenAndServe(":8080", r.Handler)

}

func handleTransaction(ctx *fasthttp.RequestCtx) {
	var cmd Transaction
	err := json.Unmarshal(ctx.PostBody(), &cmd)
	if err != nil {
		ctx.Error("Invalid request body", fasthttp.StatusBadRequest)
		log.Printf(" [-] Invalid request body \n")
		return
	}

	err = validateTransaction(&cmd)
	if err != nil {
		ctx.Error(err.Error(), fasthttp.StatusBadRequest)
		return
	}

	err = createTransaction(&cmd)
	if err != nil {
		ctx.Error(err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusCreated)
}

func validateTransaction(cmd *Transaction) error {
	balance, err := checkBalance(*cmd)
	if err != nil {
		if err == sql.ErrNoRows {
			log.Printf(" [-] User not found")
			return ErrUserNotFound
		}
		return err
	}

	if balance.Balance+cmd.Transaction < 0 {
		log.Printf(" [-] Insufficient funds")
		return ErrInsufficientFunds
	}

	return nil
}

func createTransaction(cmd *Transaction) error {
	transaction, err := json.Marshal(cmd)
	if err != nil {
		return err
	}

	err = publishMessage([]byte(transaction))
	if err != nil {
		return err
	}

	err = readMessage()
	if err != nil {
		return err
	}

	return nil
}
